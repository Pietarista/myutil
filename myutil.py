#!/usr/bin/env python3
# coding=utf-8

"""
myutil - utility to simulate 'gsutil cp -R' behavior
Uses Google Cloud service account creds from file or GOOGLE_APPLICATION_CREDENTIALS env variable
"""

import argparse
import logging
import os
import sys
from google.cloud import storage, exceptions
from google.oauth2 import service_account

logging.basicConfig(level=logging.INFO,
                    format='%(asctime)s %(name)-8s %(levelname)-8s %(message)s',
                    datefmt='%H:%M:%S',
                    filename='.myutil.log',
                    filemode='w')
CONSOLE = logging.StreamHandler(sys.stdout)
formatter = logging.Formatter('%(asctime)s %(name)-24s: %(levelname)-8s %(message)s')
formatter.datefmt = '%H:%M:%S'
CONSOLE.setFormatter(formatter)
LOGGER = logging.getLogger('myutil')
logging.getLogger().addHandler(CONSOLE)


def fail(message):
    """
    Default on-error action

    Parameters
    ----------
    message : str
        Log message
    """
    LOGGER.error('\x1b[31m' + message + '\x1b[0m')
    sys.exit(1)


def download_blob(blob_object, to_path):
    """
    Download a single blob from gs to file

    Parameters
    ----------
    blob_object : google.cloud.storage.Blob
        Object describing blob which should be downloaded
    to_path : str
        Path to directory where objects from GS should be downloaded to

    Returns
    -------

    """
    blob_path, blob_name = blob_object.name.rsplit("/", 1)
    path_to_file = os.path.join(to_path, blob_path, blob_name)
    os.makedirs(os.path.dirname(path_to_file), exist_ok=True)
    try:
        blob_object.reload()  # to make sure we have exact revision
        with open(file=path_to_file,
                  mode='wb') as to_file:
            with blob_object.open(mode='rb',
                                  chunk_size=10000000) as from_blob:
                to_file.write(from_blob.read())
                LOGGER.info(f'Copied blob {blob_object.name} into {path_to_file}')
    except OSError as ose:
        fail(f'Attempt to write to file {path_to_file} caused an error {ose.errno}: {ose.strerror}!')
    except exceptions.GoogleCloudError as gce:
        fail(f'Attempt to copy blob {blob_object.name} caused an error with code {gce.code}!')


def main():
    """
    Perform main action

    Returns
    -------

    """
    arg_parser = argparse.ArgumentParser()
    arg_parser.add_argument('from_bucket', help='Address of bucket to copy from (may be with subpath)')
    arg_parser.add_argument('to_directory', help='Path to copy to')
    arg_parser.add_argument('--creds-file', help='File with GCP service account creds')
    args = arg_parser.parse_args()
    # credentials preparation
    try:
        creds_file = args.creds_file if args.creds_file else os.environ['GOOGLE_APPLICATION_CREDENTIALS']
        credentials = service_account.Credentials.from_service_account_file(creds_file)
    except KeyError:
        fail('No creds file provided and no GOOGLE_APPLICATION_CREDENTIALS env var set!')
    except FileNotFoundError:
        fail(f'File not found: {creds_file}')

    bucket_path_raw = args.from_bucket[len("gs://"):] if args.from_bucket.startswith("gs://") else args.from_bucket
    bucket_name, bucket_path = bucket_path_raw.split("/", 1)
    LOGGER.info(f'Recursive copy from "{bucket_name}" bucket, path "{bucket_path}" to {args.to_directory} directory')
    # downloading
    storage_client = storage.client.Client(credentials=credentials)
    try:
        from_bucket = storage_client.list_blobs(bucket_or_name=bucket_name,
                                                prefix=bucket_path,
                                                versions=False).pages
    except exceptions.NotFound:
        fail(f'Bucket with name {bucket_name} not found')
    else:
        for page in from_bucket:
            for entry in page:
                download_blob(blob_object=entry,
                              to_path=args.to_directory)


if __name__ == '__main__':
    main()
